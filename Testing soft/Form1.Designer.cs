﻿namespace Testing_soft
{
    partial class Testomator
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Testomator));
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ReadTestButton = new System.Windows.Forms.Button();
            this.StartTestButton = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CreateTestButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.startTestPanel1 = new Testing_soft.StartTestPanel();
            this.creatingNewTest1 = new Testing_soft.CreatingNewTest();
            this.testViewpanel1 = new Testing_soft.TestView();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Consolas", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(623, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 33);
            this.label2.TabIndex = 1;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ReadTestButton);
            this.panel1.Controls.Add(this.StartTestButton);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.CreateTestButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 500);
            this.panel1.TabIndex = 11;
            // 
            // ReadTestButton
            // 
            this.ReadTestButton.FlatAppearance.BorderSize = 0;
            this.ReadTestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.8F);
            this.ReadTestButton.ForeColor = System.Drawing.Color.White;
            this.ReadTestButton.Image = ((System.Drawing.Image)(resources.GetObject("ReadTestButton.Image")));
            this.ReadTestButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ReadTestButton.Location = new System.Drawing.Point(5, 321);
            this.ReadTestButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ReadTestButton.Name = "ReadTestButton";
            this.ReadTestButton.Size = new System.Drawing.Size(329, 78);
            this.ReadTestButton.TabIndex = 13;
            this.ReadTestButton.Text = "Переглянути тест";
            this.ReadTestButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ReadTestButton.UseVisualStyleBackColor = true;
            this.ReadTestButton.Click += new System.EventHandler(this.ReadTestButton_Click);
            // 
            // StartTestButton
            // 
            this.StartTestButton.FlatAppearance.BorderSize = 0;
            this.StartTestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartTestButton.ForeColor = System.Drawing.Color.White;
            this.StartTestButton.Image = ((System.Drawing.Image)(resources.GetObject("StartTestButton.Image")));
            this.StartTestButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StartTestButton.Location = new System.Drawing.Point(5, 210);
            this.StartTestButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StartTestButton.Name = "StartTestButton";
            this.StartTestButton.Size = new System.Drawing.Size(329, 78);
            this.StartTestButton.TabIndex = 12;
            this.StartTestButton.Text = "Пройти тест";
            this.StartTestButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.StartTestButton.UseVisualStyleBackColor = true;
            this.StartTestButton.Click += new System.EventHandler(this.StartTestButton_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(0)))), ((int)(((byte)(14)))));
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(344, 57);
            this.panel3.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(338, 46);
            this.label1.TabIndex = 11;
            this.label1.Text = "TESTOMATOR 3000";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // CreateTestButton
            // 
            this.CreateTestButton.FlatAppearance.BorderSize = 0;
            this.CreateTestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTestButton.ForeColor = System.Drawing.Color.White;
            this.CreateTestButton.Image = ((System.Drawing.Image)(resources.GetObject("CreateTestButton.Image")));
            this.CreateTestButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CreateTestButton.Location = new System.Drawing.Point(5, 103);
            this.CreateTestButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CreateTestButton.Name = "CreateTestButton";
            this.CreateTestButton.Size = new System.Drawing.Size(329, 78);
            this.CreateTestButton.TabIndex = 11;
            this.CreateTestButton.Text = "Створити тест";
            this.CreateTestButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CreateTestButton.UseVisualStyleBackColor = true;
            this.CreateTestButton.Click += new System.EventHandler(this.CreateTestButton_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(0)))), ((int)(((byte)(14)))));
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(344, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(656, 33);
            this.panel2.TabIndex = 12;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(344, 500);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.MaximumSize = new System.Drawing.Size(1000, 500);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(656, 0);
            this.panel4.TabIndex = 13;
            this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // startTestPanel1
            // 
            this.startTestPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.startTestPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("startTestPanel1.BackgroundImage")));
            this.startTestPanel1.Location = new System.Drawing.Point(344, 33);
            this.startTestPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.startTestPanel1.MaximumSize = new System.Drawing.Size(656, 464);
            this.startTestPanel1.MinimumSize = new System.Drawing.Size(656, 464);
            this.startTestPanel1.Name = "startTestPanel1";
            this.startTestPanel1.Size = new System.Drawing.Size(656, 464);
            this.startTestPanel1.TabIndex = 15;
            this.startTestPanel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // creatingNewTest1
            // 
            this.creatingNewTest1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(46)))), ((int)(((byte)(47)))));
            this.creatingNewTest1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("creatingNewTest1.BackgroundImage")));
            this.creatingNewTest1.Location = new System.Drawing.Point(344, 33);
            this.creatingNewTest1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.creatingNewTest1.MaximumSize = new System.Drawing.Size(656, 464);
            this.creatingNewTest1.MinimumSize = new System.Drawing.Size(656, 464);
            this.creatingNewTest1.Name = "creatingNewTest1";
            this.creatingNewTest1.Size = new System.Drawing.Size(656, 464);
            this.creatingNewTest1.TabIndex = 14;
            this.creatingNewTest1.Load += new System.EventHandler(this.creatingNewTest1_Load);
            // 
            // testViewpanel1
            // 
            this.testViewpanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.testViewpanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testViewpanel1.BackgroundImage")));
            this.testViewpanel1.Location = new System.Drawing.Point(343, 33);
            this.testViewpanel1.MaximumSize = new System.Drawing.Size(656, 464);
            this.testViewpanel1.MinimumSize = new System.Drawing.Size(656, 464);
            this.testViewpanel1.Name = "testViewpanel1";
            this.testViewpanel1.Size = new System.Drawing.Size(656, 464);
            this.testViewpanel1.TabIndex = 16;
            // 
            // Testomator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1000, 500);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.creatingNewTest1);
            this.Controls.Add(this.testViewpanel1);
            this.Controls.Add(this.startTestPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(1000, 500);
            this.MinimumSize = new System.Drawing.Size(1000, 500);
            this.Name = "Testomator";
            this.Opacity = 0.95D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button CreateTestButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ReadTestButton;
        private System.Windows.Forms.Button StartTestButton;
        private System.Windows.Forms.Panel panel4;
        private CreatingNewTest creatingNewTest1;
        private StartTestPanel startTestPanel1;
        private TestView testViewpanel1;
    }
}

