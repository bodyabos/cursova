﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing_soft
{
    public partial class TestName : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

       

        public TestName()
        {

            InitializeComponent();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void TestName_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        private void TestName_Load(object sender, EventArgs e)
        {
            while (name.Text == null && comboBox1.Text == null)
            {
                AddNameButton.Visible = false;
            }
        }

        public void AddNameButton_Click(object sender, EventArgs e)
        {
            using (StreamWriter fstream = new StreamWriter(Testomator.path, true, System.Text.Encoding.Default))
            {

                fstream.WriteLine("Назва тесту:" + name.Text);
                fstream.WriteLine("Час на виконання:" + comboBox1.Text);

            }

           
            this.Close();

        }

        public MultipleChoises MultipleChoises
        {
            get => default(MultipleChoises);
            set
            {
            }
        }

        public NewFormNoChoises NewFormNoChoises
        {
            get => default(NewFormNoChoises);
            set
            {
            }
        }

        public NewFormOneChoise NewFormOneChoise
        {
            get => default(NewFormOneChoise);
            set
            {
            }
        }

        public Conect Conect
        {
            get => default(Conect);
            set
            {
            }
        }
    }



            





    
}
