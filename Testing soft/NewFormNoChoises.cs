﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing_soft
{
    public partial class NewFormNoChoises : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public NewFormNoChoises()
        {

            InitializeComponent();
        }
       
        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void NewFormNoChoises_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void NewFormNoChoises_Load(object sender, EventArgs e)
        {

        }

        private void AddNewQueButton_Click(object sender, EventArgs e)
        {
            using (StreamWriter fstream = new StreamWriter(Testomator.path, true, System.Text.Encoding.Default))
            {
                fstream.WriteLine($"Номер питання:{Testomator.Questions}");
                fstream.WriteLine("Тип питання:1");
                fstream.WriteLine("Питання:" + question.Text);
                fstream.WriteLine("Відповідь:" + answer.Text);

            }

            Testomator.Questions++;
            this.Close();
        }
    }
}
