﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing_soft
{
    public partial class FinalTest : Form
    {
       
        public FinalTest()
        {
            InitializeComponent();
            timer1.Start();


            ReadQuestion();

        }

        public string correctAns;

        private int choosenAns = 0;
        private string choosenAns1;
        private string correct;
        public bool stop = true;

        public int ChoosenAns { get => choosenAns; set => choosenAns = value; }
        public string Correct { get => correct; set => correct = value; }
        public string ChoosenAns1 { get => choosenAns1; set => choosenAns1 = value; }


        public bool answer = false;

        

        Match m = new Regex(@"Тип питання:(.+)").Match(System.IO.File.ReadAllText(StartTestPanel.FilePath));
        Match m1 = new Regex(@"Тип питання:1(?:\n|\r|\r\n)Питання:(.+)(?:\n|\r|\r\n)Відповідь:(.+)").Match(System.IO.File.ReadAllText(StartTestPanel.FilePath));
        Match m2 = new Regex(@"Тип питання:2(?:\n|\r|\r\n)Питання:(.+)(?:\n|\r|\r\n)Кількість варіантів:(\d)(?:\n|\r|\r\n)(?:Відповідь:(.+)(?:\n|\r|\r\n))+Правильна відповідь:(.+)").Match(System.IO.File.ReadAllText(StartTestPanel.FilePath));
        Match m3 = new Regex(@"Тип питання:3(?:\n|\r|\r\n)Питання:(.+)(?:\n|\r|\r\n)Кількість варіантів:(.+)(?:\n|\r|\r\n)(?:Відповідь:(.+)(?:\n|\r|\r\n))+Правильна відповідь:(.+)").Match(System.IO.File.ReadAllText(StartTestPanel.FilePath));
        Match m4 = new Regex(@"Тип питання:4(?:\n|\r|\r\n)Питання:Відповідність(?:\n|\r|\r\n)(.+)-(.+)(?:\n|\r|\r\n)(.+)-(.+)(?:\n|\r|\r\n)(.+)-(.+)").Match(System.IO.File.ReadAllText(StartTestPanel.FilePath));

        public void ReadQuestion()
        {
            while (m.Success && stop)
            {

                while (m1.Success && stop)
                {

                    question1Label.Text = m1.Groups[1].Value;

                    question1Label.Visible = true;
                    textBox1.Visible = true;
                    button1.Visible = true;
                    correctAns = m1.Groups[2].Value;

                    m1 = m1.NextMatch();

                    answer = false;
                    stop = false;

                }




                while (m2.Success && stop)

                {

                    label11.Visible = true;
                    label11.Text = m2.Groups[1].Value;
                   
                    
                    
                    if (Int32.Parse(m2.Groups[2].Value) == 2)
                    {


                        label12.Visible = true;
                        label12.Text = m2.Groups[3].Captures[0].Value;
                        label13.Visible = true;
                        label13.Text = m2.Groups[3].Captures[1].Value;
                        label12.ForeColor = Color.White;
                        label13.ForeColor = Color.White;
                       
                        answer = false;
                        stop = false;
                    }
                    if (Int32.Parse(m2.Groups[2].Value) == 3)
                    {
                        label12.Visible = true;
                        label12.Text = m2.Groups[3].Captures[0].Value;
                        label13.Visible = true;
                        label13.Text = m2.Groups[3].Captures[1].Value;
                        label14.Visible = true;
                        label14.Text = m2.Groups[3].Captures[2].Value;
                        label12.ForeColor = Color.White;
                        label13.ForeColor = Color.White;
                        label14.ForeColor = Color.White;

                        
                        stop = false;
                        answer = false;
                    }
                    if (Int32.Parse(m2.Groups[2].Value) == 4)
                    {
                        label12.Visible = true;
                        label12.Text = m2.Groups[3].Captures[0].Value;
                        label13.Visible = true;
                        label13.Text = m2.Groups[3].Captures[1].Value;
                        label14.Visible = true;
                        label14.Text = m2.Groups[3].Captures[2].Value;
                        label15.Visible = true;
                        label15.Text = m2.Groups[3].Captures[3].Value;
                        label12.ForeColor = Color.White;
                        label13.ForeColor = Color.White;
                        label14.ForeColor = Color.White;
                        label15.ForeColor = Color.White;
                        
                        stop = false;
                        answer = false;
                    }
                    if (Int32.Parse(m2.Groups[2].Value) == 5)
                    {
                        label12.Visible = true;
                        label12.Text = m2.Groups[3].Captures[0].Value;
                        label13.Visible = true;
                        label13.Text = m2.Groups[3].Captures[1].Value;
                        label14.Visible = true;
                        label14.Text = m2.Groups[3].Captures[2].Value;
                        label15.Visible = true;
                        label15.Text = m2.Groups[3].Captures[3].Value;
                        label16.Visible = true;
                        label16.Text = m2.Groups[3].Captures[4].Value;
                        label12.ForeColor = Color.White;
                        label13.ForeColor = Color.White;
                        label14.ForeColor = Color.White;
                        label15.ForeColor = Color.White;
                        label16.ForeColor = Color.White;
                      
                        stop = false;
                        answer = false;
                    }
                    if (Int32.Parse(m2.Groups[2].Value) == 6)
                    {
                        label12.Visible = true;
                        label12.Text = m2.Groups[3].Captures[0].Value;
                        label13.Visible = true;
                        label13.Text = m2.Groups[3].Captures[1].Value;
                        label14.Visible = true;
                        label14.Text = m2.Groups[3].Captures[2].Value;
                        label15.Visible = true;
                        label15.Text = m2.Groups[3].Captures[3].Value;
                        label16.Visible = true;
                        label16.Text = m2.Groups[3].Captures[4].Value;
                        label17.Visible = true;
                        label17.Text = m2.Groups[3].Captures[5].Value;
                        label12.ForeColor = Color.White;
                        label13.ForeColor = Color.White;
                        label14.ForeColor = Color.White;
                        label15.ForeColor = Color.White;
                        label16.ForeColor = Color.White;
                        label17.ForeColor = Color.White;
                       
                        stop = false;
                        answer = false;
                    }
                    Correct = m2.Groups[4].Value;
                    button2.Visible = true;
                    m2 = m2.NextMatch();


                }

                while (m3.Success && stop)
                {

                    label7.Visible = true;
                    label7.Text = m3.Groups[1].Value;
                    question1Label.Visible = false;
                    textBox1.Visible = false;
                    button1.Visible = false;
                    if (Int32.Parse(m3.Groups[2].Value) == 2)
                    {


                        label6.Visible = true;
                        label6.Text = m3.Groups[3].Captures[0].Value;
                        label5.Visible = true;
                        label5.Text = m3.Groups[3].Captures[1].Value;
                        label6.ForeColor = Color.White;
                        label5.ForeColor = Color.White;

                      

                        answer = false;
                        stop = false;
                    }
                    if (Int32.Parse(m3.Groups[2].Value) == 3)
                    {
                        label6.Visible = true;
                        label6.Text = m3.Groups[3].Captures[0].Value;
                        label5.Visible = true;
                        label5.Text = m3.Groups[3].Captures[1].Value;
                        label4.Visible = true;
                        label4.Text = m3.Groups[3].Captures[2].Value;
                        label6.ForeColor = Color.White;
                        label5.ForeColor = Color.White;
                        label4.ForeColor = Color.White;

                       
                        answer = false;
                        stop = false;
                    }
                    if (Int32.Parse(m3.Groups[2].Value) == 4)
                    {
                        label6.Visible = true;
                        label6.Text = m3.Groups[3].Captures[0].Value;
                        label5.Visible = true;
                        label5.Text = m3.Groups[3].Captures[1].Value;
                        label4.Visible = true;
                        label4.Text = m3.Groups[3].Captures[2].Value;
                        label3.Visible = true;
                        label3.Text = m3.Groups[3].Captures[3].Value;
                        label6.ForeColor = Color.White;
                        label5.ForeColor = Color.White;
                        label4.ForeColor = Color.White;
                        label3.ForeColor = Color.White;
                       
                        answer = false;
                        stop = false;
                    }
                    if (Int32.Parse(m3.Groups[2].Value) == 5)
                    {
                        label6.Visible = true;
                        label6.Text = m3.Groups[3].Captures[0].Value;
                        label5.Visible = true;
                        label5.Text = m3.Groups[3].Captures[1].Value;
                        label4.Visible = true;
                        label4.Text = m3.Groups[3].Captures[2].Value;
                        label3.Visible = true;
                        label3.Text = m3.Groups[3].Captures[3].Value;
                        label2.Visible = true;
                        label2.Text = m3.Groups[3].Captures[4].Value;
                        label6.ForeColor = Color.White;
                        label5.ForeColor = Color.White;
                        label4.ForeColor = Color.White;
                        label3.ForeColor = Color.White;
                        label2.ForeColor = Color.White;
                        
                        answer = false;
                        stop = false;
                    }
                    if (Int32.Parse(m3.Groups[2].Value) == 6)
                    {
                        label6.Visible = true;
                        label6.Text = m3.Groups[3].Captures[0].Value;
                        label5.Visible = true;
                        label5.Text = m3.Groups[3].Captures[1].Value;
                        label4.Visible = true;
                        label4.Text = m3.Groups[3].Captures[2].Value;
                        label3.Visible = true;
                        label3.Text = m3.Groups[3].Captures[3].Value;
                        label2.Visible = true;
                        label2.Text = m3.Groups[3].Captures[4].Value;
                        label1.Visible = true;
                        label1.Text = m3.Groups[3].Captures[5].Value;
                        label6.ForeColor = Color.White;
                        label5.ForeColor = Color.White;
                        label4.ForeColor = Color.White;
                        label3.ForeColor = Color.White;
                        label2.ForeColor = Color.White;
                        label1.ForeColor = Color.White;
                        
                        answer = false;
                        stop = false;
                    }
                    Correct = m3.Groups[4].Value;
                    button3.Visible = true;
                    m3 = m3.NextMatch();

                }

                while (m4.Success && stop)
                    {
                    button4.Visible = true;
                        comboBox1.Visible = true;
                        comboBox2.Visible = true;
                        comboBox3.Visible = true;
                        comboBox4.Visible = true;
                        comboBox5.Visible = true;
                        comboBox6.Visible = true;

                        comboBox1.Items.Add(m4.Groups[5].Value);
                        comboBox1.Items.Add(m4.Groups[3].Value);
                        comboBox1.Items.Add(m4.Groups[1].Value);

                        comboBox3.Items.Add(m4.Groups[1].Value);
                        comboBox3.Items.Add(m4.Groups[3].Value);
                        comboBox3.Items.Add(m4.Groups[5].Value);

                        comboBox5.Items.Add(m4.Groups[5].Value);
                        comboBox5.Items.Add(m4.Groups[1].Value);
                        comboBox5.Items.Add(m4.Groups[3].Value);

                        comboBox2.Items.Add(m4.Groups[4].Value);
                        comboBox2.Items.Add(m4.Groups[2].Value);
                        comboBox2.Items.Add(m4.Groups[6].Value);

                        comboBox4.Items.Add(m4.Groups[2].Value);
                        comboBox4.Items.Add(m4.Groups[4].Value);
                        comboBox4.Items.Add(m4.Groups[6].Value);

                        comboBox6.Items.Add(m4.Groups[6].Value);
                        comboBox6.Items.Add(m4.Groups[2].Value);
                        comboBox6.Items.Add(m4.Groups[4].Value);
                    

                    answer = false;
                    stop = false;
                    m4 = m4.NextMatch();
                }


                m.NextMatch();
                stop = false;

            }
            if(m.Success==false) endTest();

        }


            public void FinalTest_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Bounds = Screen.PrimaryScreen.Bounds;

            

        }

        private void label1_Click(object sender, EventArgs e)
        {
            endTest();
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {

            timeLabel.Text = "Часу залишилось: " + $"{--Testomator.TimeToDo}";
            if (Testomator.TimeToDo == 0) endTest();
        }

        private void timeLabel_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


        }

        private void label12_Click(object sender, EventArgs e)
        {
            label12.ForeColor = Color.Aqua;
            label13.ForeColor = Color.White;
            label14.ForeColor = Color.White;
            label15.ForeColor = Color.White;
            label16.ForeColor = Color.White;
            label17.ForeColor = Color.White;
            choosenAns = 1;


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click_1(object sender, EventArgs e)
        {
            label12.ForeColor = Color.White;
            label13.ForeColor = Color.Aqua;
            label14.ForeColor = Color.White;
            label15.ForeColor = Color.White;
            label16.ForeColor = Color.White;
            label17.ForeColor = Color.White;
            choosenAns = 2;
        }

        private void label14_Click_1(object sender, EventArgs e)
        {
            label12.ForeColor = Color.White;
            label13.ForeColor = Color.White;
            label14.ForeColor = Color.Aqua;
            label15.ForeColor = Color.White;
            label16.ForeColor = Color.White;
            label17.ForeColor = Color.White;
            choosenAns = 3;
        }

        private void label15_Click_1(object sender, EventArgs e)
        {
            label12.ForeColor = Color.White;
            label13.ForeColor = Color.White;
            label14.ForeColor = Color.White;
            label15.ForeColor = Color.Aqua;
            label16.ForeColor = Color.White;
            label17.ForeColor = Color.White;
            choosenAns = 4;
        }

        private void label16_Click_1(object sender, EventArgs e)
        {
            label12.ForeColor = Color.White;
            label13.ForeColor = Color.White;
            label14.ForeColor = Color.White;
            label15.ForeColor = Color.White;
            label16.ForeColor = Color.Aqua;
            label17.ForeColor = Color.White;
            choosenAns = 5;
        }

        private void label17_Click_1(object sender, EventArgs e)
        {
            label12.ForeColor = Color.White;
            label13.ForeColor = Color.White;
            label14.ForeColor = Color.White;
            label15.ForeColor = Color.White;
            label16.ForeColor = Color.White;
            label17.ForeColor = Color.Aqua;
            choosenAns = 6;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (choosenAns == Int32.Parse(Correct)) Testomator.PointsPlus += 1;
            else Testomator.PointsMinus += 1;
            pointsMinusLabel.Text = "Неправильних відповідей:" + Convert.ToString(Testomator.PointsMinus);
            pointsPlusLabel.Text = "Правильних відповідей:" + Convert.ToString(Testomator.PointsPlus);
            stop = true;
            answer = true;
            
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
            label16.Visible = false;
            label17.Visible = false;
            button2.Visible = false;

            ReadQuestion();

        }

        private void label6_Click(object sender, EventArgs e)
        {
            choosenAns1 += "1";
            label6.ForeColor = Color.Aqua;
            label6.ForeColor = Color.Aqua;
            label6.ForeColor = Color.Aqua;
        }

        private void label5_Click(object sender, EventArgs e)
        {
            choosenAns1 += "2";
            label5.ForeColor = Color.Aqua;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            choosenAns1 += "3";
            label4.ForeColor = Color.Aqua;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            choosenAns1 += "4";
            label3.ForeColor = Color.Aqua;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            choosenAns1 += "5";
            label2.ForeColor = Color.Aqua;
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            choosenAns1 += "6";
            label1.ForeColor = Color.Aqua;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(choosenAns1) == Int32.Parse(Correct)) Testomator.PointsPlus += 1;
            else Testomator.PointsMinus += 1;
            pointsMinusLabel.Text = "Неправильних відповідей:" + Convert.ToString(Testomator.PointsMinus);
            pointsPlusLabel.Text = "Правильних відповідей:" + Convert.ToString(Testomator.PointsPlus);
            stop = true;
            ReadQuestion();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBox1.Text == correctAns)
            {
                Testomator.PointsPlus += 1;
            }
            else Testomator.PointsMinus += 1;
            question1Label.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            stop = true;
            answer = true;
           
            pointsMinusLabel.Text = "Неправильних відповідей:" + Convert.ToString(Testomator.PointsMinus);
            pointsPlusLabel.Text = "Правильних відповідей:" + Convert.ToString(Testomator.PointsPlus);
            ReadQuestion();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Visible = false;
            comboBox1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            comboBox4.Visible = false;
            comboBox5.Visible = false;
            comboBox6.Visible = false;
            if ((comboBox1.SelectedIndex == 1 && comboBox2.SelectedIndex == 3) || (comboBox1.SelectedIndex == 2 && comboBox2.SelectedIndex == 1) || (comboBox1.SelectedIndex == 3 && comboBox2.SelectedIndex == 2))
            {
                Testomator.PointsPlus += 1;
            }
            else Testomator.PointsMinus += 1;

            if ((comboBox2.SelectedIndex == 1 && comboBox4.SelectedIndex == 1) || (comboBox2.SelectedIndex == 2 && comboBox4.SelectedIndex == 2) || (comboBox2.SelectedIndex == 3 && comboBox4.SelectedIndex == 3))
            {
                Testomator.PointsPlus += 1;
            }
            else Testomator.PointsMinus += 1;

            if ((comboBox5.SelectedIndex == 1 && comboBox6.SelectedIndex == 1) || (comboBox5.SelectedIndex == 2 && comboBox6.SelectedIndex == 2) || (comboBox5.SelectedIndex == 3 && comboBox6.SelectedIndex == 3))
            {
                Testomator.PointsPlus += 1;
            }
            else Testomator.PointsMinus += 1;
            pointsMinusLabel.Text = "Неправильних відповідей:" + Convert.ToString(Testomator.PointsMinus);
            pointsPlusLabel.Text = "Правильних відповідей:" + Convert.ToString(Testomator.PointsPlus);

            stop = true;
            answer = true;

            ReadQuestion();
        }

        public void endTest()
        {
            double percent = 0;
            if (Testomator.PointsMinus + Testomator.PointsPlus != 0)
            {
                percent = (float)Testomator.PointsPlus / ((float)Testomator.PointsMinus + (float)Testomator.PointsPlus);
            }
            timer1.Stop();
            MessageBox.Show($"Відсоток правильних відповідей: {percent*100:F2}%", $"Часу залишилось: {Testomator.TimeToDo}");
            this.Close();
        }
    }
}
