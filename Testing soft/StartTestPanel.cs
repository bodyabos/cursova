﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Testing_soft
{
    public partial class StartTestPanel : UserControl
    {
        public StartTestPanel()
        {
            InitializeComponent();
        }

        OpenFileDialog ofd = new OpenFileDialog();

        private void StartTestPanel_Load(object sender, EventArgs e)
        {

        }
        private  string content;

        private static string filePath;

        public static string FilePath { get => filePath; set => filePath = value; }

        private void AddNewQueButton_Click(object sender, EventArgs e)
        {
            ofd.Filter = "TXT|*.txt";
            if (ofd.ShowDialog() == DialogResult.OK) {
                content = System.IO.File.ReadAllText(ofd.FileName);
                FilePath = ofd.FileName;

                Regex regex = new Regex(@"Назва тесту:(.+)\nЧас на виконання:(.+)");

                Match m = regex.Match(content);

                label2.Text = $"{m.Groups[1].Value}\n\nЧасу на виконання: {m.Groups[2].Value}";

                Testomator.TimeToDo = Int32.Parse(m.Groups[2].Value)*60;
                StartButton.Visible = true;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            FinalTest form = new FinalTest();
            form.Show();

        }

        public FinalTest FinalTest
        {
            get => default(FinalTest);
            set
            {
            }
        }
    }
}
