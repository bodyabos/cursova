﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing_soft
{
    public partial class MultipleChoises : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public MultipleChoises()
        {
            InitializeComponent();
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            textBox6.Visible = false;
            checkBox3.Visible = false;
            checkBox4.Visible = false;
            checkBox5.Visible = false;
            checkBox6.Visible = false;
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void MultipleChoises_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value == 2)
            {
                label11.Visible = false;
                label12.Visible = false;
                label13.Visible = false;
                label14.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                checkBox3.Visible = false;
                checkBox4.Visible = false;
                checkBox5.Visible = false;
                checkBox6.Visible = false;
            }
            if (numericUpDown1.Value == 3)
            {
                label11.Visible = true;
                label12.Visible = false;
                label13.Visible = false;
                label14.Visible = false;
                textBox3.Visible = true;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                checkBox3.Visible = true;
                checkBox4.Visible = false;
                checkBox5.Visible = false;
                checkBox6.Visible = false;
            }
            if (numericUpDown1.Value == 4)
            {
                label11.Visible = true;
                label12.Visible = true;
                label13.Visible = false;
                label14.Visible = false;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = false;
                textBox6.Visible = false;
                checkBox3.Visible = true;
                checkBox4.Visible = true;
                checkBox5.Visible = false;
                checkBox6.Visible = false;
            }
            if (numericUpDown1.Value == 5)
            {
                label11.Visible = true;
                label12.Visible = true;
                label13.Visible = true;
                label14.Visible = false;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = false;
                checkBox3.Visible = true;
                checkBox4.Visible = true;
                checkBox5.Visible = true;
                checkBox6.Visible = false;
            }
            if (numericUpDown1.Value == 6)
            {
                label11.Visible = true;
                label12.Visible = true;
                label13.Visible = true;
                label14.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = true;
                checkBox3.Visible = true;
                checkBox4.Visible = true;
                checkBox5.Visible = true;
                checkBox6.Visible = true;
            }
        }

        private void AddNewQueButton_Click(object sender, EventArgs e)
        {
            using (StreamWriter fstream = new StreamWriter(Testomator.path, true, System.Text.Encoding.Default))
            {
                string correctAns = "";
                fstream.WriteLine($"Номер питання:{Testomator.Questions}");
                fstream.WriteLine("Тип питання:3");
                fstream.WriteLine("Питання:" + question.Text);
                fstream.WriteLine("Кількість варіантів:" + numericUpDown1.Value);

                if (numericUpDown1.Value == 2) { fstream.WriteLine("Відповідь:" + textBox1.Text); fstream.WriteLine("Відповідь:" + textBox2.Text); }
                if (numericUpDown1.Value == 3) { fstream.WriteLine("Відповідь:" + textBox1.Text); fstream.WriteLine("Відповідь:" + textBox2.Text); fstream.WriteLine("Відповідь:" + textBox3.Text); }
                if (numericUpDown1.Value == 4) { fstream.WriteLine("Відповідь:" + textBox1.Text); fstream.WriteLine("Відповідь:" + textBox2.Text); fstream.WriteLine("Відповідь:" + textBox3.Text); fstream.WriteLine("Відповідь:" + textBox4.Text); }
                if (numericUpDown1.Value == 5) { fstream.WriteLine("Відповідь:" + textBox1.Text); fstream.WriteLine("Відповідь:" + textBox2.Text); fstream.WriteLine("Відповідь:" + textBox3.Text); fstream.WriteLine("Відповідь:" + textBox4.Text); fstream.WriteLine("Відповідь:" + textBox5.Text); }
                if (numericUpDown1.Value == 6) { fstream.WriteLine("Відповідь:" + textBox1.Text); fstream.WriteLine("Відповідь:" + textBox2.Text); fstream.WriteLine("Відповідь:" + textBox3.Text); fstream.WriteLine("Відповідь:" + textBox4.Text); fstream.WriteLine("Відповідь:" + textBox5.Text); fstream.WriteLine("Відповідь:" + textBox6.Text); }
                if (checkBox1.Checked == true) correctAns = correctAns + "1";
                if (checkBox2.Checked == true) correctAns = correctAns + "2";
                if (checkBox3.Checked == true) correctAns = correctAns + "3";
                if (checkBox4.Checked == true) correctAns = correctAns + "4";
                if (checkBox5.Checked == true) correctAns = correctAns + "5";
                if (checkBox6.Checked == true) correctAns = correctAns + "6";
                fstream.WriteLine("Правильні відповіді:" + correctAns);

            }

            Testomator.Questions++;

            this.Close();
        }
    }
}
