﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Permissions;
using System.Security;
using System.Text.RegularExpressions;

namespace Testing_soft
{
    public partial class CreatingNewTest : UserControl
    {
        public CreatingNewTest()
        {

            InitializeComponent();
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";

        }



        public static string path = Application.UserAppDataPath + "\\test.txt";
        public void AddNewQueButton_Click(object sender, EventArgs e)
        {
            if (!File.Exists(path))
            {
                File.Create(path);

                TestName form1 = new TestName();
                form1.Show();
            }
            else
            {



                if (rbNoChoice.Checked)
                {
                    NewFormNoChoises form = new NewFormNoChoises();
                    form.Show();
                }
                if (rbOneChoice.Checked)
                {
                    NewFormOneChoise form = new NewFormOneChoise();
                    form.Show();
                }
                if (rbMultipleChoice.Checked)
                {
                    MultipleChoises form = new MultipleChoises();
                    form.Show();
                }

                if (rbConnectChoises.Checked)
                {
                    Conect form = new Conect();
                    form.Show();
                }
                

            }
            Update();
        }

        public void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
            

        }
        string content;
        public void CreatingNewTest_Load(object sender, EventArgs e)
        {
            
            dataGridView1.ColumnCount = 3;
            contentLoad();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (File.Exists(path))
            {
                dataGridView1.Rows.Clear();
                contentLoad();
                Regex regex1 = new Regex(@"Номер питання:(.+)\nТип питання:(.+)\nПитання:(.+)");

                Match m1 = regex1.Match(content);
               
                while (m1.Success)
                {
                    
                    dataGridView1.Rows.Add(m1.Groups[1].Value, m1.Groups[2].Value, m1.Groups[3].Value);

                    m1 = m1.NextMatch();
                

                }
            }
        }

        private void saveTestButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;
            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, content);
            MessageBox.Show("Файл збережено");
            File.Delete( Application.UserAppDataPath + "\\test.txt");
        }

        private void contentLoad()
        {
            if (File.Exists(path))
            {

                Regex regex = new Regex(@"Назва тесту:(.+)\nЧас на виконання:(.+)");
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    content = (sr.ReadToEnd());
                }
                Match m = regex.Match(content);

                label2.Text = m.Groups[1].Value;
                


            }
        }

        public TestName TestName
        {
            get => default(TestName);
            set
            {
            }
        }
    }
}
