﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Testing_soft
{
    public partial class Testomator : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private static int questions = 1;
        private static int timeToDo = 1;
        private static int pointsPlus = 0;
        private static int pointsMinus = 0;


        public static int Questions { get => questions; set => questions = value; }
        public static int TimeToDo { get => timeToDo; set => timeToDo = value; }
        public static int PointsPlus { get => pointsPlus; set => pointsPlus = value; }
        public static int PointsMinus { get => pointsMinus; set => pointsMinus = value; }

        public static string path = Application.UserAppDataPath + "\\test.txt";
        public Testomator()
        {

           
            InitializeComponent();
            creatingNewTest1.BringToFront();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void CreateTestButton_Click(object sender, EventArgs e)
        {
            startTestPanel1.Visible = false;
            testViewpanel1.Visible = false;
            creatingNewTest1.Visible = true;
            

        }

        private void StartTestButton_Click(object sender, EventArgs e)
        {
            creatingNewTest1.Visible = false;
            startTestPanel1.Visible = true;
            testViewpanel1.Visible = false;

        }

        private void ReadTestButton_Click(object sender, EventArgs e)
        {
            creatingNewTest1.Visible = false;
            startTestPanel1.Visible = false;
            testViewpanel1.Visible = true;

        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void creatingNewTest1_Load(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        internal Properties.Resources Resources
        {
            get => default(Properties.Resources);
            set
            {
            }
        }

        internal Properties.Settings Settings
        {
            get => default(Properties.Settings);
            set
            {
            }
        }

        public CreatingNewTest CreatingNewTest
        {
            get => default(CreatingNewTest);
            set
            {
            }
        }

        public StartTestPanel StartTestPanel
        {
            get => default(StartTestPanel);
            set
            {
            }
        }

        public TestView TestView
        {
            get => default(TestView);
            set
            {
            }
        }
    }
}
