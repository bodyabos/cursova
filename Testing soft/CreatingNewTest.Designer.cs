﻿namespace Testing_soft
{
    partial class CreatingNewTest
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreatingNewTest));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.AddNewQueButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbConnectChoises = new System.Windows.Forms.RadioButton();
            this.rbMultipleChoice = new System.Windows.Forms.RadioButton();
            this.rbOneChoice = new System.Windows.Forms.RadioButton();
            this.rbNoChoice = new System.Windows.Forms.RadioButton();
            this.saveTestButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(28, 50);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.Size = new System.Drawing.Size(599, 153);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.FillWeight = 20.15743F;
            this.Column1.HeaderText = "№";
            this.Column1.MinimumWidth = 10;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 91.37056F;
            this.Column2.HeaderText = "Тип питання";
            this.Column2.MinimumWidth = 60;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 188.472F;
            this.Column3.HeaderText = "Питання";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(21, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "Список питань:";
            // 
            // AddNewQueButton
            // 
            this.AddNewQueButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddNewQueButton.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.AddNewQueButton.Location = new System.Drawing.Point(328, 295);
            this.AddNewQueButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddNewQueButton.Name = "AddNewQueButton";
            this.AddNewQueButton.Size = new System.Drawing.Size(131, 32);
            this.AddNewQueButton.TabIndex = 2;
            this.AddNewQueButton.Text = "Додати питання";
            this.AddNewQueButton.UseVisualStyleBackColor = true;
            this.AddNewQueButton.Click += new System.EventHandler(this.AddNewQueButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbConnectChoises);
            this.groupBox1.Controls.Add(this.rbMultipleChoice);
            this.groupBox1.Controls.Add(this.rbOneChoice);
            this.groupBox1.Controls.Add(this.rbNoChoice);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Location = new System.Drawing.Point(27, 229);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(281, 186);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Виберіть тип питання:";
            // 
            // rbConnectChoises
            // 
            this.rbConnectChoises.AutoSize = true;
            this.rbConnectChoises.Location = new System.Drawing.Point(7, 142);
            this.rbConnectChoises.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbConnectChoises.Name = "rbConnectChoises";
            this.rbConnectChoises.Size = new System.Drawing.Size(157, 29);
            this.rbConnectChoises.TabIndex = 3;
            this.rbConnectChoises.TabStop = true;
            this.rbConnectChoises.Text = "Відповідності";
            this.rbConnectChoises.UseVisualStyleBackColor = true;
            // 
            // rbMultipleChoice
            // 
            this.rbMultipleChoice.AutoSize = true;
            this.rbMultipleChoice.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.rbMultipleChoice.Location = new System.Drawing.Point(7, 106);
            this.rbMultipleChoice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbMultipleChoice.Name = "rbMultipleChoice";
            this.rbMultipleChoice.Size = new System.Drawing.Size(250, 29);
            this.rbMultipleChoice.TabIndex = 2;
            this.rbMultipleChoice.TabStop = true;
            this.rbMultipleChoice.Text = "З багатьма варіантами";
            this.rbMultipleChoice.UseVisualStyleBackColor = true;
            // 
            // rbOneChoice
            // 
            this.rbOneChoice.AutoSize = true;
            this.rbOneChoice.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.rbOneChoice.Location = new System.Drawing.Point(7, 70);
            this.rbOneChoice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbOneChoice.Name = "rbOneChoice";
            this.rbOneChoice.Size = new System.Drawing.Size(214, 29);
            this.rbOneChoice.TabIndex = 1;
            this.rbOneChoice.TabStop = true;
            this.rbOneChoice.Text = "З одним варіантом";
            this.rbOneChoice.UseVisualStyleBackColor = true;
            // 
            // rbNoChoice
            // 
            this.rbNoChoice.AutoSize = true;
            this.rbNoChoice.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.rbNoChoice.Location = new System.Drawing.Point(7, 30);
            this.rbNoChoice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbNoChoice.Name = "rbNoChoice";
            this.rbNoChoice.Size = new System.Drawing.Size(237, 29);
            this.rbNoChoice.TabIndex = 0;
            this.rbNoChoice.TabStop = true;
            this.rbNoChoice.Text = "Без варіантів відповіді";
            this.rbNoChoice.UseVisualStyleBackColor = true;
            // 
            // saveTestButton
            // 
            this.saveTestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveTestButton.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.saveTestButton.Location = new System.Drawing.Point(495, 383);
            this.saveTestButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveTestButton.Name = "saveTestButton";
            this.saveTestButton.Size = new System.Drawing.Size(131, 32);
            this.saveTestButton.TabIndex = 4;
            this.saveTestButton.Text = "Зберегти тест";
            this.saveTestButton.UseVisualStyleBackColor = true;
            this.saveTestButton.Click += new System.EventHandler(this.saveTestButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(344, 229);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 36);
            this.label2.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.button1.Location = new System.Drawing.Point(495, 208);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Оновити данні";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CreatingNewTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(46)))), ((int)(((byte)(47)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saveTestButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AddNewQueButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(656, 464);
            this.MinimumSize = new System.Drawing.Size(656, 464);
            this.Name = "CreatingNewTest";
            this.Size = new System.Drawing.Size(656, 464);
            this.Load += new System.EventHandler(this.CreatingNewTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddNewQueButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbConnectChoises;
        private System.Windows.Forms.RadioButton rbMultipleChoice;
        private System.Windows.Forms.RadioButton rbOneChoice;
        private System.Windows.Forms.RadioButton rbNoChoice;
        private System.Windows.Forms.Button saveTestButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button button1;
    }
}
